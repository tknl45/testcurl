# this code demo how to check api value to decide CI pass or failed

> Use `curl` to call rest api.

> And then using `Python` to fetch json specfied attribute( id ).

> check id is right


# 在Pipeline 中如何使用curl ＋ Python 去讀取rest api中的參數值，並決定該job成功與失敗

> 使用 `curl` 呼叫rest api取得資料

> 使用 `Python` 讀取 json 資料中的id值.

> 使用if 去判斷id值是否正確來決定該job是否成功



## failed 失敗

https://gitlab.com/tknl45/testcurl/-/jobs/120846967

https://gitlab.com/tknl45/testcurl/blob/fcea10e49f1430e2c11c1e93e8d5185dbd2937d3/.gitlab-ci.yml


## passed 通過

https://gitlab.com/tknl45/testcurl/-/jobs/120849225

https://gitlab.com/tknl45/testcurl/blob/f20a2a69dc4a9154604ad72da48b3772e8db9f79/.gitlab-ci.yml